var clientConnection,
    ttUsername = 'ramesh.m.m@ideas2it.com',
    password = 'Test@1234',
    communicationOrganizationId = 'cQvpaarcGQyq3O4YZcoqoWuN',
    tigerTextUrl = 'https://api.tigertext.me';

connectTigerClient(ttUsername);
logout();

 function connectTigerClient(ttUsername) {
    if (!clientConnection) {
        clientConnection = new TigerConnect.Client({
            baseUrl: tigerTextUrl,
            defaultOrganizationId : communicationOrganizationId,
            partnerName: 'react'
        });
        chatLogin(ttUsername, password);
    }
}

function chatLogin(userName, password) {
    console.log('Signing In---- ------');
    clientConnection.signIn(userName, password).then(function(session) {
        console.log('Signed in as ', session.user.displayName);
        clientConnection.events.connect();
    }, function(err) {
        console.log('Error Signing in:', err.message || err.code)
    });
}

function sendMessageToConversation(refferBotUserName, messageBody) {
	clientConnection.messages.sendToUser(refferBotUserName, messageBody).then(function(message){
        console.log('sendToUser--- ------');
    }).catch(console.log);
}

function logout() {
	window.addEventListener("beforeunload", function(event) {
        if (!clientConnection) {
            clientConnection.signOut().then(function(){
                 console.log('Signed out successfully.');
            });
        }
	});
}

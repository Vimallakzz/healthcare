import React from 'react';
import { connect } from 'react-redux';
import { withRouter, RouteComponentProps } from 'react-router';

import { userLogoutRequest } from './actions/user';
import Header from './components/Header';
import { IReduxState } from './reducers';
import { AppRoutes, AuthRoutes } from './routes';

interface IStateProps {
  isUserLoggedIn: boolean
}

interface IDispatchProps {
  logoutUser: () => void
}

type Props = IStateProps & IDispatchProps & RouteComponentProps;

const App = ({ isUserLoggedIn, logoutUser }: Props) => {
  return (
    <main>
      {
        isUserLoggedIn ?
          <>
            <Header logoutUser={logoutUser} />
            <div className="app-body">
              <div className="app-content">
                <AppRoutes />
              </div>
            </div>
          </> : <AuthRoutes />
      }
    </main>
  );
};
const mapStateToProps = (state: IReduxState): IStateProps => ({
  isUserLoggedIn: state.user.isUserLoggedIn
});

const mapDispatchToProps: IDispatchProps = {
  logoutUser: userLogoutRequest
};

export default connect<IStateProps, IDispatchProps, {}, IReduxState>(
  mapStateToProps, mapDispatchToProps
)(withRouter(App));

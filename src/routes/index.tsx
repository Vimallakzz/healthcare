import React from 'react';
import { Route, RouteProps, Switch, Redirect } from 'react-router-dom';
import { APP_ROUTES, AUTH_ROUTES, homePath } from './constants';

export const AppRoutes = () =>
  (<Switch>
    {APP_ROUTES.map((route: RouteProps, index: number) => (
      <Route
        path={route.path}
        exact={route.exact}
        key={index}
        component={route.component}
      />
    ))}
    <Redirect to={homePath} />} />
  </Switch>);

export const AuthRoutes = () =>
  (<Switch>
    {AUTH_ROUTES.map((route: RouteProps, index: number) => (
      <Route
        path={route.path}
        exact={route.exact}
        key={index}
        component={route.component}
      />
    ))}
  </Switch>);

import { RouteProps } from 'react-router-dom';

import Counter from '../pages/Counter';
import HealthCareLogin from '../pages/HealthcareLogin';
import FaceSheet from '../pages/Facesheet';
import Home from '../pages/Home';
import Patient from '../pages/Patient';

export const homePath = '/home';

export const AUTH_ROUTES: RouteProps[] = [
  {
    path: '/',
    exact: true,
    component: HealthCareLogin
  },
  {
    path: '/login',
    exact: true,
    component: HealthCareLogin
  }
];

export const APP_ROUTES: RouteProps[] = [
  {
    path: homePath,
    exact: true,
    component: Home
  },
  {
    path: '/counter',
    exact: true,
    component: Counter
  },
  {
    path: '/home/facesheet',
    exact: true,
    component: FaceSheet
  },
  {
    path: '/patients',
    exact: true,
    component: Patient
  }
];

import patientActions from '../constants/actionTypes/patient';
import { IActionProps } from '.';

export interface IPatient {
  [key: string]: any;
  firstName: string;
  lastName: string;
  policyNumber: string;
  insurance: string;
  addressLine1: string;
  addressLine2?: string;
  state?: string;
  city: string;
  zipcode?: string;
}

export interface IPatientReducer {
  patients: IPatient[];
}

const initialState: IPatientReducer = {
  patients: [
    {firstName: 'Roger',  lastName: 'Fernandez', insurance: 'HIP Insurance Group',
      policyNumber: '934SIGL03048KADFN', addressLine1: '991 Quinten Run Apt. 139', city: 'Skylabury'},
    {firstName: 'Tony',  lastName: 'Barnes', insurance: 'Coventry Corp. Group',
      policyNumber: '3694BE7009DAF21B9', addressLine1: '9283 Kaya Camp', city: 'Schambergerland'},
    {firstName: 'Isaac',  lastName: 'West', insurance: 'Highmark Group',
      policyNumber: '6396C4FB8FA2A8942', addressLine1: '3260 Cassandra Common', city: 'Port Dereckchester'},
    {firstName: 'Celia',  lastName: 'Griffith', insurance: 'Centene Corp. Group',
      policyNumber: '4371E40C32DF2B098', addressLine1: '695 Breanne Circle Suite 651', city: 'Hudsonhaven'},
    {firstName: 'Nettie',  lastName: 'Maxwell', insurance: 'Kaiser Foundation Group',
      policyNumber: '12500D2749A3B2331', addressLine1: '517 Lakin Groves', city: 'Lake Devon'},
    {firstName: 'Eliza',  lastName: 'Day', insurance: 'Aetna Group',
      policyNumber: '3694BE7009DAF21B9', addressLine1: '351 Robb Shore', city: 'Lake Anissaport'},
    {firstName: 'Charles',  lastName: 'Gibson', insurance: 'Health Net of California, Inc.',
      policyNumber: '6396C4FB8FA2A8942', addressLine1: '920 Alec Spurs', city: 'North Ruben'},
    {firstName: 'Birdie',  lastName: 'Brewer', insurance: 'Cigna Health Group',
      policyNumber: '4371E40C32DF2B098', addressLine1: '8891 Bud Spurs Apt. 111', city: 'West Adela'},
    {firstName: 'Tillie',  lastName: 'Nelson', insurance: `California Physicians' Service`,
      policyNumber: '12500D2749A3B2331', addressLine1: '90 Dietrich Knoll Apt. 961', city: 'Lake Josie'},
    {firstName: 'Adeline',  lastName: 'Munoz', insurance: 'HIP Insurance Group',
      policyNumber: '4371E40C32DF2B098', addressLine1: '9708 Bruen Cliffs', city: 'Marcusland'},
    {firstName: 'Jordan',  lastName: 'Hopkins', insurance: 'Unitedhealth Group',
      policyNumber: '12500D2749A3B2331', addressLine1: '1432 Nader Rapids', city: 'Lake Koby'},
    {firstName: 'Scott',  lastName: 'Horton', insurance: 'HIP Insurance Group',
      policyNumber: '3694BE7009DAF21B9', addressLine1: '461 Lucy Mountains Suite 650', city: 'Lake Myrtis'},
    {firstName: 'Melvin',  lastName: 'Sharp', insurance: 'HCSC Group',
      policyNumber: '6396C4FB8FA2A8942', addressLine1: '743 Rosemarie Parkway', city: 'North Camronberg'},
    {firstName: 'Katie',  lastName: 'Lane', insurance: 'HIP Insurance Group',
      policyNumber: '4371E40C32DF2B098', addressLine1: '1824 Kuvalis Drive Suite 288', city: 'Janellemouth'},
    {firstName: 'Alejandro',  lastName: 'Adams', insurance: 'Blue Cross Blue Shield of New Jersey Group',
      policyNumber: '12500D2749A3B2331', addressLine1: '8625 Hickle Fort Apt. 943', city: 'Carolineton'},
    {firstName: 'Mildred',  lastName: 'Bowen', insurance: 'Wellpoint Inc. Group',
      policyNumber: '4371E40C32DF2B098', addressLine1: '1533 Fisher Run Suite 950', city: 'Bellstad'},
    {firstName: 'Calvin',  lastName: 'Greene', insurance: 'Health Net of California, Inc.',
      policyNumber: '12500D2749A3B2331', addressLine1: '6663 Mertz Groves Suite 884', city: 'Lake Edgardo'}
  ]
};

export const patientReducer = (state: IPatientReducer = initialState, action: IActionProps): IPatientReducer => {
  switch (action.type) {
    case patientActions.ADD_PATIENT:
      return {
        ...state,
        patients: [  action.patient, ...state.patients]
      };
    default:
      return state;
  }
};

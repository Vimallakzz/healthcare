import COUNTERTYPES from '../constants/actionTypes/counter';
import { IActionProps } from '.';

export interface ICounterReducer {
  count: number,
  incrementBy: number,
  decrementBy: number
}

const initialState: ICounterReducer = {
  count: 0,
  incrementBy: 1,
  decrementBy: 1
};

export const counterReducer = (
    state: ICounterReducer = initialState,
    action: IActionProps
  ): ICounterReducer => {
  switch (action.type) {
    case COUNTERTYPES.INCREMENT_COUNT_REQUEST:
      return {
        ...state,
        count: state.count + state.incrementBy
      };
    case COUNTERTYPES.DECREMENT_COUNT_REQUEST:
      return {
        ...state,
        count: state.count - state.decrementBy
      };
    default:
      return state;
  }
};

import { connectRouter, RouterState } from 'connected-react-router';
import { counterReducer, ICounterReducer } from './counter';
import { userReducer, IUserReducer } from './user';
import  { patientReducer, IPatientReducer } from './patient';
import { History } from 'history';
import { combineReducers, Reducer } from 'redux';


export interface IActionProps {
  type: string,
  [key: string]: any
}

export interface IReduxState {
  counter: ICounterReducer,
  user: IUserReducer,
  patient: IPatientReducer,
  router: RouterState
}

const rootReducer = (history: History): Reducer<IReduxState> => combineReducers({
  counter: counterReducer,
  user: userReducer,
  patient: patientReducer,
  router: connectRouter(history)
});

export default rootReducer;

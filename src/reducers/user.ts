import USERTYPES from '../constants/actionTypes/user';
import Storage from '../global/Storage';
import { IActionProps } from '.';

export interface IUserReducer {
  name: string,
  loading: boolean,
  isUserLoggedIn: boolean,
  email: string
}

const initialState: IUserReducer = {
  name: '',
  email: '',
  loading: false,
  isUserLoggedIn: Boolean(Storage.getItem('authtoken'))
};

export const userReducer = (state: IUserReducer = initialState, action: IActionProps): IUserReducer => {
  const { response } = action;
  switch (action.type) {
    case USERTYPES.USER_LOGIN_REQUEST:
      return {
        ...state,
        loading: true
      };
    case USERTYPES.USER_LOGIN_SUCCESS:
      return {
        ...state,
        email: response.email,
        isUserLoggedIn: true,
        loading: false,
        name: response.name
      };
    case USERTYPES.USER_LOGIN_FAIL:
      return {
        ...state,
        loading: false
      };
    case USERTYPES.USER_LOGOUT_REQUEST:
      return {
        ...state,
        loading: true
      };
    case USERTYPES.USER_LOGOUT_SUCCESS:
      return {
        ...state,
        email: '',
        isUserLoggedIn: false,
        loading: false,
        name: ''
      };
    case USERTYPES.USER_LOGOUT_FAIL:
      return {
        ...state,
        loading: false
      };
    default:
      return state;
  }
};

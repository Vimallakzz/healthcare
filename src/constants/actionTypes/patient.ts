import keyMirror from 'key-mirror';

const PATIENT_ACTIONS = keyMirror({
  ADD_PATIENT: null
});

export default PATIENT_ACTIONS;

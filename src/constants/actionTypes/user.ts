import keyMirror from 'key-mirror';

const USERTYPES = keyMirror({
  USER_LOGIN_REQUEST: null,
  USER_LOGIN_SUCCESS: null,
  USER_LOGIN_FAIL: null,

  USER_LOGOUT_REQUEST: null,
  USER_LOGOUT_SUCCESS: null,
  USER_LOGOUT_FAIL: null
});

export default USERTYPES;

import { takeLatest } from 'redux-saga/effects';

import { COUNTERTYPES, USERTYPES } from '../constants/actionTypes';
import * as counterSaga from './counter';
import * as userSaga from './user';

export function* rootSaga() {
  yield takeLatest(COUNTERTYPES.INCREMENT_COUNT_REQUEST, counterSaga.incrementCount);
  yield takeLatest(COUNTERTYPES.DECREMENT_COUNT_REQUEST, counterSaga.decrementCount);
  yield takeLatest(USERTYPES.USER_LOGIN_REQUEST, userSaga.userLogin);
  yield takeLatest(USERTYPES.USER_LOGOUT_REQUEST, userSaga.userLogout);
}

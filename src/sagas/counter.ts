import { put } from 'redux-saga/effects';
import Toast from '../components/Toaster';
import * as counterActions from '../actions/counter';

export function* incrementCount() {
  try {
    Toast('success', 'Count incremented');
    yield put(counterActions.incrementCountSuccess());
  } catch (e) {
    yield put(counterActions.incrementCountFailure());
  }
}

export function* decrementCount() {
  try {
    Toast('success', 'Count decremented');
    yield put(counterActions.decrementCountSuccess());
  } catch (e) {
    yield put(counterActions.decrementCountFailure());
  }
}

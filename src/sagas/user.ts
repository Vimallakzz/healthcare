import { push } from 'connected-react-router';
import { put } from 'redux-saga/effects';

import * as userActions from '../actions/user';
import Storage from '../global/Storage';

export function* userLogin(action: any) {
  try {
    const { email }: { email: string } = action;
    Storage.setItem('authtoken', new Date().getUTCMilliseconds());
    yield put(userActions.userLoginSuccess({ name: 'sachidhanandhan', email }));
    yield put(push('/home'));
  } catch (e) {
    yield put(userActions.userLoginFail({ message: 'Error occured while login' }));
  }
}

export function* userLogout() {
  try {
    Storage.deleteItem('authtoken');
    yield put(userActions.userLogoutSuccess());
    yield put(push('/'));
  } catch (e) {
    yield put(userActions.userLogoutFail());
  }
}

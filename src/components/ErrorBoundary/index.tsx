import React from 'react';

interface IErrorBoundaryState {
  hasError: boolean
}

interface IErrorBoundaryProps {
  children: any
}

export default class ErrorBoundary extends React.Component<IErrorBoundaryProps, IErrorBoundaryState> {

  public static getDerivedStateFromError() {
    return { hasError: true };
  }
  constructor(props: IErrorBoundaryProps) {
    super(props);
    this.state = { hasError: false };
  }

  public componentDidCatch(error: any, info: any) {
    // you can log your error and info here
  }

  public render() {
    const { hasError } = this.state;
    return hasError ? <h1>Something went wrong.</h1> : this.props.children;
  }
}

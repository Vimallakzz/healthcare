import { ReactNode } from 'react';
import cogoToast from 'cogo-toast';

interface IToastOptions {
  hideAfter?: number;
  position?: string;
  heading?: string;
  renderIcon?: any;
  bar?: object;
  onClick?: any;
  role?: string;
  toastContainerID?: string;
}

/**
 * Toast component allow you to add notification to your app with ease.
 */
const Toast = (status: string, message: string | ReactNode, options?: IToastOptions) =>
  cogoToast[status](message, {
    position: 'top-right',
    ...options
  });

export default Toast;

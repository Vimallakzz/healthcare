import React from 'react';
import { connect } from 'react-redux';
import { NavLink, Link } from 'react-router-dom';
import { Dropdown } from 'react-bootstrap';

import { userLogoutRequest } from '../../actions/user';
import settingsIcon from '../../assets/images/settings.svg';
import caretDown from '../../assets/images/caret-down.svg';
import logo from '../../assets/images/logo-large.png';

import styles from './header.module.scss';
import { IReduxState } from '../../reducers';

interface IHeaderProps {
  logoutUser: () => void
}

interface IHeaderState {
  collapsed: boolean;
}

interface IDispatchProps {
  logoutUser: () => void;
}

interface INavItem {
  route: string;
  label: string;
}

type CombinedProps = IDispatchProps & IHeaderProps;

class Header extends React.Component<CombinedProps, IHeaderState> {
  private navItems: INavItem[] = [
    { route: '/home', label: 'Home'},
    { route: '/patients', label: 'Patients'}
    // { route: '/inbox', label: 'Message'}
  ];

  // @ts-ignore
  private renderSettingsToggler = React.forwardRef(({ children, onClick }, ref) => (
    <div
      // @ts-ignore
      ref={ref}
      className="d-flex align-items-center cursor-pointer px-1"
      onClick={e => {
        e.preventDefault();
        onClick(e);
      }}
    >
      <img src={settingsIcon} className={styles.settingsIcon} alt="settings" />
      <span className={`text-white ml-1 mr-2 ${styles.settings}`}>{children}</span>
      <img src={caretDown} className={styles.dropdownIcon} alt="v" />
    </div>
  ));

  constructor(props: CombinedProps) {
    super(props);
    this.state = {
      collapsed: true
    };
  }

  public render() {
    const { logoutUser } = this.props;
    const { collapsed } = this.state;
    const menuClass = collapsed ?
      'collapse navbar-collapse' :
      'collapse navbar-collapse show';
    const togglerClass = collapsed ?
      'navbar-toggler navbar-toggler-right collapsed' :
      'navbar-toggler navbar-toggler-right';
    return (
      <nav className={`navbar navbar-expand-md bg-primary navbar-dark ${styles.navbarCustom} ${styles.navbar}`}>
        <Link className={`navbar-brand d-flex align-items-center text-white ${styles.brand}`} to="/">
          <img src={logo} className="mr-2" alt="logo" />ABC Healthcare
        </Link>
        <button
          className={`navbar-toggler ${togglerClass}`}
          onClick={this.toggleMenuCollapse}
          type="button"
          data-toggle="collapse"
          aria-controls="navbarText"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>
        <div className={`collapse navbar-collapse ${menuClass}`}>
         {this.renderNavItems()}
          <span className="navbar-text">
            <Dropdown>
              <Dropdown.Toggle as={this.renderSettingsToggler} id="dropdown-custom-components">
                Settings
              </Dropdown.Toggle>

              <Dropdown.Menu bsPrefix="dropdown-menu settings-menu">
                <Dropdown.Item href="#" eventKey="logout" onSelect={logoutUser}>Log out</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </span>
        </div>
      </nav>
    );
  }

  private renderNavItems = () => {
    return (
      <ul className={`navbar-nav justify-content-center ${styles.navList}`}>
        {this.navItems.map(({route, label} : INavItem, index: number) => (
          <li
            className={`nav-item
              ${ index === 0 ? 'mr-md-2 mr-lg-4' : 'mx-md-2 mx-lg-4'}
              my-md-0 ${styles.navItem}
            ${index === 0 ? 'mb-2 mt-3' : 'my-2'}`}
            key={`${label}-${route}`}
          >
            <NavLink
              className={`nav-link ${styles.navLink}`}
              activeClassName={styles.active}
              to={route}
            >{label}</NavLink>
          </li>
        ))}
      </ul>
    );
  }

  private toggleMenuCollapse = () => {
    this.setState((prevState: IHeaderState) => ({ collapsed: !prevState.collapsed }));
  }
}

const mapDispatchToProps: IDispatchProps = {
  logoutUser: userLogoutRequest
};

export default connect<null, IDispatchProps, CombinedProps, IReduxState>(null, mapDispatchToProps)(Header);

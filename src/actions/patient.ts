import PATIENT_ACTION from '../constants/actionTypes/patient';
import { IPatient } from '../reducers/patient';

export const addPatient = (patient: IPatient) => ({
  type: PATIENT_ACTION.ADD_PATIENT,
  patient
});

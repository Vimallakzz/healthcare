import COUNTERTYPES from '../constants/actionTypes/counter';

export const incrementCountRequest = () => ({
  type: COUNTERTYPES.INCREMENT_COUNT_REQUEST
});

export const incrementCountSuccess = () => ({
  type: COUNTERTYPES.INCREMENT_COUNT_SUCCESS
});

export const incrementCountFailure = () => ({
  type: COUNTERTYPES.INCREMENT_COUNT_FAILURE
});

export const decrementCountRequest = () => ({
  type: COUNTERTYPES.DECREMENT_COUNT_REQUEST
});

export const decrementCountSuccess = () => ({
  type: COUNTERTYPES.DECREMENT_COUNT_SUCCESS
});

export const decrementCountFailure = () => ({
  type: COUNTERTYPES.DECREMENT_COUNT_FAILURE
});

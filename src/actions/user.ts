import USERTYPES from '../constants/actionTypes/user';

export const userLoginRequest = (email: string, password: string) => ({
  type: USERTYPES.USER_LOGIN_REQUEST,
  email,
  password
});

export const userLoginSuccess = (response: any) => ({
  type: USERTYPES.USER_LOGIN_SUCCESS,
  response
});

export const userLoginFail = (error: any) => ({
  type: USERTYPES.USER_LOGIN_FAIL,
  error
});

export const userLogoutRequest = () => ({
  type: USERTYPES.USER_LOGOUT_REQUEST
});

export const userLogoutSuccess = () => ({
  type: USERTYPES.USER_LOGOUT_SUCCESS
});

export const userLogoutFail = () => ({
  type: USERTYPES.USER_LOGOUT_FAIL
});

interface IError {
  name: string;
  message: string;
}

/**
 * ApiError with additional properties i.e name and message
 * @type {module.ApiError}
 */
export default class ApiError extends Error {
  constructor(error: IError) {
    super();
    this.name = error.name;
    this.message = error.message;
    this.stack = Error().stack;
  }
}

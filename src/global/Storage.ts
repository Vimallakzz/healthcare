interface IStorageService {
  setItem: (key: string, value: any) => void,
  getItem: (key: string) => any,
  deleteItem: (key: string) => void,
  clearAllItem: () => void
}

/**
 * Util Class to handle storage services
 * Localstorage, SessionStorage can be handled
 */
class StorageService implements IStorageService {

  /**
   * Store the value against key in browser local storage
   * @param {String} key    Key string to refer the value
   * @param {any}    value  Data to save
   */
  public setItem(key: string, value: any): void {
    localStorage.setItem(key, value);
  }

  /**
   * To read the value from local storage for given key.
   * @param {String} key Used to read the data from storage
   */
  public getItem(key: string): any {
    return localStorage.getItem(key);
  }

  /**
   * To remove specific key value pair from local storage
   * @param {String} key Key name to delete
   */
  public deleteItem(key: string): void {
    localStorage.removeItem(key);
  }

  /**
   * To remove all items from local storage
   */
  public clearAllItem(): void {
    localStorage.clear();
  }
}

export default new StorageService();

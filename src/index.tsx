import './styles/app.global.scss';

import { ConnectedRouter, routerMiddleware } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { applyMiddleware, compose, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';

import App from './App';
import ErrorBoundary from './components/ErrorBoundary';
// import AxiosInterceptor from './global/AxiosInterceptor';
import rootReducer from './reducers';
import { rootSaga } from './sagas';
import * as serviceWorker from './serviceWorker';



// create the saga middleware
const sagaMiddleware = createSagaMiddleware();

const composeEnhancer: typeof compose = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const history = createBrowserHistory();
// create the store
export const store = createStore(
  rootReducer(history),
  composeEnhancer(applyMiddleware(routerMiddleware(history), sagaMiddleware))
);

// then run the saga
sagaMiddleware.run(rootSaga);
// AxiosInterceptor.setup();

const Main = () => (
  <Provider store={store}>
    <ErrorBoundary>
      <ConnectedRouter history={history}>
        <App />
      </ConnectedRouter>
    </ErrorBoundary>
  </Provider>);

ReactDOM.render(<Main />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

import React, { ReactElement } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { ResponsiveContainer, BarChart, XAxis, YAxis, Bar } from 'recharts';
import caretRight from '../../assets/images/caret-right.svg';

import styles from './home.module.scss';
import moment from 'moment';
import { IReduxState } from '../../reducers';
import { IPatient } from '../../reducers/patient';

interface IStateProps {
  patients: IPatient[]
}

type IHomeProps = IStateProps;

interface IHomeState {
  dailyPatientReport: IPatientReport[];
  insuranceProviders: IInsuranceProvider[];
}

interface IComponentsMap {
  colProperty: string;
  label: string;
  contentRenderer: () => ReactElement;
  headerComponents?: any;
}

interface IFormMenu {
  label: string;
  route?: string;
  onClick?: (e: any) => void;
}

interface IPatientColumn {
  label: string;
  key: string;
  order: number;
  formatter?: any;
}

interface IPatientReport {
  date: string,
  patients: number
}

interface IWeeklyPatients {
  lastWeek: number;
  thisWeek: number;
}

interface IInsuranceProvider {
  provider: string;
  value: number;
}

class Home extends React.Component<IHomeProps, IHomeState> {
  private componentsMap: IComponentsMap[] = [
    { colProperty: 'col-12 col-sm-6 col-lg-3', label: 'Forms',
      contentRenderer: () => this.renderFormsMenu() },
    { colProperty: 'col-12 col-sm-6 col-lg-4', label: 'Weekly Patient Report',
      contentRenderer: () => this.renderPatientReport() },
    { colProperty: 'col-12 col-lg-5', label: 'Top Insurance Provider',
      contentRenderer: () => this.renderInsuranceProvider() },
    { colProperty: 'col-12',
      label: 'Recently Added Patients',
      contentRenderer: () => this.renderPatientsData(),
      headerComponents:  [ () => this.renderViewAll() ]}
  ];

  private patientColumns: IPatientColumn[] = [
    { label: 'PATIENT NAME', key: 'firstName', order: 1,
      formatter: (firstName: string, patient: IPatient) => this.formatName(firstName, patient) },
    { label: 'INSURANCE', key: 'insurance', order: 2 },
    { label: 'POLICY NUMBER', key: 'policyNumber', order: 3 },
    { label: 'ADDRESS', key: 'addressLine1', order: 4,
      formatter: (addressLine1: string, patient: IPatient) => this.formatAddress(addressLine1, patient)},
    { label: 'CITY', key: 'city', order: 5 }
  ];

  private formsMenu: IFormMenu[] = [
    { label: 'Face Sheet', route: '/home/facesheet'},
    { label: 'Medication Profile', route: '/'},
    { label: 'Diagnosis Form', route: '/'},
    { label: 'Visit Note', route: '/'},
    { label: 'Evaluation', route: '/'}
  ];

  private weeklyPatientReport: IWeeklyPatients = {
    lastWeek: 12, thisWeek: 18
  };

  constructor(props: IHomeProps) {
    super(props);
    this.state = {
      dailyPatientReport: [
        { date: moment().day(-7).toString(), patients: 4 },
        { date: moment().day(-6).toString(), patients: 4 },
        { date: moment().day(-5).toString(), patients: 3 },
        { date: moment().day(-4).toString(), patients: 9 },
        { date: moment().day(-3).toString(), patients: 5 },
        { date: moment().day(-2).toString(), patients: 2 },
        { date: moment().day(-1).toString(), patients: 1 }
      ],
      insuranceProviders: [
        { provider: 'Independence Blue Cross Group', value: 178 },
        { provider: 'Wellpoint Inc. Group', value: 146 },
        { provider: 'Coventry Corp. Group', value: 45 },
        { provider: `California Physicians' Service`, value: 28 },
        { provider: 'Highmark Group', value: 12 }
      ]
    };
  }

  public render() {
    return (
      <div className={styles.home}>
        <div className="row m-0">
          {this.componentsMap.map(
            (componentProps: IComponentsMap) =>
              this.renderGrid(componentProps)
          )}
        </div>
      </div>
    );
  }

  private renderFormsMenu = () => (
    <div>
      {this.formsMenu.map(({ route, label}: IFormMenu) => (
        <div key={label} className={styles.formMenu}>
          <Link className={styles.formMenuLink} to={route as string}>{label}</Link>
        </div>
      ))}
    </div>)

  private renderInsuranceProvider = () => {
    const { insuranceProviders } = this.state;
    return (
      <div className={styles.insuranceProviders}>
        <ResponsiveContainer width="100%" height={230}>
          <BarChart data={insuranceProviders} margin={{left: 210, top: -15}} layout="vertical">
            <XAxis type="number" hide={true} dataKey="value" />
            <YAxis
              dataKey="provider"
              tickLine={false}
              axisLine={false}
              type="category"
              interval={0}
              tick={<CustomizedProviderTick data={insuranceProviders}/>}
            />
            {/* <Tooltip /> */}
            <Bar dataKey="value" fill="#4998cb" barSize={10} />
          </BarChart>
        </ResponsiveContainer>
      </div>);
  }

  private renderPatientReport = () => {
    const { dailyPatientReport } = this.state;
    const {lastWeek, thisWeek} = this.weeklyPatientReport;
    return (
      <div className={styles.weeklyReport}>
        <div className={`d-flex`}>
          <div className={`${styles.reportBrief} pr-md-3 pr-3`}>
            <div className={styles.count}>{lastWeek}</div>
            <div className={styles.period}>Last week</div>
            <div className={styles.subText}># of Patients</div>
          </div>
          <div className={`${styles.reportBrief} px-md-3 px-3`}>
            <div className={styles.count}>{thisWeek}</div>
            <div className={styles.period}>This week</div>
            <div className={styles.subText}># of Patients</div>
          </div>
          <div className={`${styles.reportBrief} pl-md-3 pl-3`}>
            <div className={`${styles.count} ${styles.success}`}>
              {`${Math.ceil((lastWeek / thisWeek) * 100)}%`}
            </div>
            <div className={styles.period}>increase</div>
            <div className={styles.subText}># of Patients</div>
          </div>
        </div>
        <ResponsiveContainer width="100%" height={165}>
          <BarChart data={dailyPatientReport} margin={{bottom: 20}}>
            <XAxis
              dataKey="date"
              tickLine={false}
              axisLine={false}
              interval={0}
              tick={<CustomizedAxisTick />}
            />
            <YAxis hide={true} dataKey="patients" />
            {/* <Tooltip /> */}
            <Bar dataKey="patients" fill="#4998cb" barSize={10} />
          </BarChart>
        </ResponsiveContainer>
      </div>
    );
  }

  private renderPatientsData = () => {
    const { patients } = this.props;
    return (
      <div className={styles.patientTableContainer}>
        <table className={styles.patientTable}>
          <thead>
            <tr>
              {this.patientColumns.map(({ key, label }: IPatientColumn) => (
                <th key={key}>{label}</th>
              ))}
            </tr>
          </thead>
          <tbody>
            {patients.slice(0, 8).map((patient: any) => (
              <tr key={patient.id}>
                {this.patientColumns.map(({ key, formatter }: IPatientColumn) => (
                  <td key={key}>{formatter ? formatter(patient[key], patient) : patient[key] || ''}</td>
                ))}
              </tr>
            ))}
          </tbody>
        </table>
      </div>);
  }

  private renderGrid = ({label, contentRenderer, colProperty, headerComponents}: IComponentsMap) => (
    <div className={`${styles.gridContainer} ${colProperty}`}>
      <div className={styles.header}>
        <span className={styles.label}>{label}</span>
        {headerComponents && headerComponents.length ? (
          headerComponents.map((compoentRenderer: any) => compoentRenderer())
        ) : null}
      </div>
      <div className={styles.gridContent}>{contentRenderer()}</div>
    </div>
  )

  private renderViewAll = () => (
    <Link className={styles.viewAll} to="/patients">
      View All
      <img src={caretRight} className={styles.caretRight} alt=">" />
    </Link>
  )

  private formatName = (firstName: string, { lastName }: IPatient) =>
    `${firstName}${lastName ? ` ${lastName}` : ''}`

  private formatAddress = (addressLine1: string, { addressLine2 }: IPatient) =>
    `${addressLine1}${addressLine2 ? `, ${addressLine2}` : ''}`
}

const CustomizedAxisTick = (props: any) => {
  const {
    x, y, payload
  } = props;
  const momentObj = moment(payload.value);
  return (
    <g transform={`translate(${x},${y})`}>
      <text x={0} y={0} dy={16} textAnchor="middle" fill="#666" fontSize="14" transform="rotate(0)">
        <tspan x="0" dy="1.2em">{momentObj.format('D')}</tspan>
        <tspan x="0" dy="1.2em">{momentObj.format('MMM')}</tspan>
      </text>
    </g>
  );
};

const CustomizedProviderTick = (props: any) => {
  const {
    x, y, payload, data
  } = props;
  const targetValue = data.find(({ provider }: IInsuranceProvider) => provider === payload.value).value;

  return (
    <g transform={`translate(${x},${y})`}>
      <text x={0} y={0} dx={-260} textAnchor="start" fill="#101b4f" fontSize="14" transform="rotate(0)">
        <tspan x="0" y="5" >{payload.value}</tspan>
        <tspan x="-5" y="5" dx={-20} fill="#4998cb" fontWeight="bold">{targetValue}</tspan>
      </text>
    </g>
  );
};

const mapStateToProps = (state: IReduxState): IStateProps => ({
  patients: state.patient.patients
});

export default connect<IStateProps, {}, {}, IReduxState>(mapStateToProps, {})(Home);

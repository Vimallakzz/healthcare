import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { IReduxState } from '../../reducers';
import { IPatient } from '../../reducers/patient';
import searchIcon from '../../assets/images/search.svg';
import addIcon from '../../assets/images/add.svg';

import styles from './Patient.module.scss';

interface IStateProps {
  patients: IPatient[];
}

interface IPatientsState {
  search: string;
}

interface IPatientColumn {
  label: string;
  key: string;
  order: number;
  formatter?: any;
}

type combinedProps = IStateProps;

class Patients extends React.Component<combinedProps, IPatientsState> {
  private patientColumns: IPatientColumn[] = [
    { label: 'PATIENT NAME', key: 'firstName', order: 1,
      formatter: (firstName: string, patient: IPatient) => this.formatName(firstName, patient) },
    { label: 'INSURANCE', key: 'insurance', order: 2 },
    { label: 'POLICY NUMBER', key: 'policyNumber', order: 3 },
    { label: 'ADDRESS', key: 'addressLine1', order: 4,
      formatter: (addressLine1: string, patient: IPatient) => this.formatAddress(addressLine1, patient)},
    { label: 'CITY', key: 'city', order: 5 }
  ];

  private searchFields: string[] = [
    'firstName',
    'lastName',
    'insurance',
    'policyNumber',
    'city',
    'addressLine1',
    'addressLine2'
  ];

  constructor(props: combinedProps) {
    super(props);
    this.state = {
      search: ''
    };
  }

  public render() {
    const { patients } = this.props;
    const { search } = this.state;
    return (
      <div className={`${styles.gridContainer}`}>
        <div className={styles.header}>
          <span className={styles.label}>Patients</span>
          <div className="d-flex align-items-center">
            <Link className={styles.addNew} to="/home/facesheet">
              <img src={addIcon} className={styles.addIcon} alt="+" />
              New Patient
            </Link>
            <div className={styles.searchContainer}>
              <img src={searchIcon} className={styles.searchIcon} alt="search"/>
              <input
                type="text"
                placeholder="Search.."
                value={search}
                onChange={this.onSearch}
                className={styles.searchInput}
              />
            </div>
          </div>
        </div>
        <div className={styles.gridContent}>
          <div className={styles.patientTableContainer}>
            <table className={styles.patientTable}>
              <thead>
                <tr>
                  {this.patientColumns.map(({ key, label }: IPatientColumn) => (
                    <th key={key}>{label}</th>
                  ))}
                </tr>
              </thead>
              <tbody>
                {this.applySearch(patients).map((patient: any) => (
                  <tr key={patient.id}>
                    {this.patientColumns.map(({ key, formatter }: IPatientColumn) => (
                      <td key={key}>{formatter ? formatter(patient[key], patient) : patient[key] || ''}</td>
                    ))}
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>);
  }

  private formatName = (firstName: string, { lastName }: IPatient) =>
    `${firstName}${lastName ? ` ${lastName}` : ''}`

  private formatAddress = (addressLine1: string, { addressLine2 }: IPatient) =>
    `${addressLine1}${addressLine2 ? `, ${addressLine2}` : ''}`

  private applySearch = (patients: IPatient[]) => {
    const { search } = this.state;
    if (!search) {
      return patients;
    }
    return patients.filter((patient: IPatient) => this.searchFields.reduce(
      (isMatch: any, key: string) =>
        isMatch || (typeof patient[key] === 'string' && (patient[key] as string).match(RegExp(search, 'i'))), false));
  }

  private onSearch = (e: any) =>
    this.setState({ search: e.target.value })
}

const mapStateToProps = (state: IReduxState): IStateProps => ({
  patients: state.patient.patients
});

export default connect<IStateProps, {}, {}, IReduxState>(mapStateToProps, {})(Patients);

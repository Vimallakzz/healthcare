import React from 'react';
import Button from 'react-bootstrap/Button';
import { connect } from 'react-redux';
import * as counterActions from '../../actions/counter';
import { IReduxState } from '../../reducers';

interface IDispatchProps {
  incrementCount: () => void,
  decrementCount: () => void
}

interface IStateProps {
  count: number
}

type Props = IDispatchProps & IStateProps;

const Counter = ({ incrementCount, decrementCount, count }: Props)  => (
  <div className="row container">
    <Button onClick={incrementCount} className=" offset-3 col-2"> Increment </Button>
      <div className="col-2 text-center">{count}</div>
    <Button onClick={decrementCount} className="col-2"> Decrement </Button>
  </div>
);

const mapStateToProps = (state: IReduxState): IStateProps => ({
  count: state.counter.count
});

const mapDispatchToProps = (dispatch: any): IDispatchProps => ({
  decrementCount: () => dispatch(counterActions.decrementCountRequest()),
  incrementCount: () => dispatch(counterActions.incrementCountRequest())
});

export default connect<IStateProps, IDispatchProps, {}, IReduxState>(
  mapStateToProps,
  mapDispatchToProps
  )(Counter);

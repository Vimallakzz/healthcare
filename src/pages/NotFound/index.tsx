import * as React from 'react';

const NoMatch = () => (
  <div>
    No Match found
  </div>
);

export default NoMatch;

import './healthcareLogin.scss';

import React, { Component } from 'react';
import { connect } from 'react-redux';

import { userLoginRequest } from '../../actions/user';
import logo from '../../assets/images/logo-large@2x.png';
import openEye from '../../assets/images/password-view-off@3x.png';
import closedEye from '../../assets/images/showPasswordoff.jpeg';
import { IReduxState } from '../../reducers';

interface IDispatchProps {
  loginUser: (email: string, password: string) => void;
}


interface IHealthCareLoginState {
  username: string;
  password: string;
  showPassword: boolean;
}

type compinedProps = {} & IDispatchProps;

class HealthCareLogin extends Component<compinedProps, IHealthCareLoginState> {
  constructor(props: compinedProps) {
    super(props);
    this.state = {
      username: '',
      password: '',
      showPassword: false
    };
  }

  public render() {
    const { username, password, showPassword } = this.state;
    return (
      <section className="login-bg">
        <div className="login-card">
          <div className="brand-logo d-flex">
            <img src={logo} className="logo" alt="logo" />
            <h1 className="brandName">ABC Healthcare</h1>
          </div>
          <form onSubmit={this.onSubmitForm} className="login-form">
            <div className="form-group">
              <label className="form-label">User Name</label>
              <input type="text" name="username" value={username} className="form-control"
                onChange={this.changeState} />
            </div>
            <div className="form-group">
              <label className="form-label">Password</label>
              <div className="position-relative">
                <input
                  type={showPassword ? 'text' : 'password'}
                  name="password"
                  value={password}
                  className="form-control"
                  onChange={this.changeState} />
                {password && <img alt="hide/show" src={showPassword ? closedEye : openEye} className="eye-icon cursor-pointer"
                  onClick={this.handleToggle} />}
              </div>
            </div>
            <div>
              <span className="forgot-password">Forgot the password?</span>
              <button type="submit" className="btn btn-login">LOGIN</button>
            </div>
          </form>
        </div>
      </section>
    );
  }

  private changeState = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    this.setState({
      [name]: value
    } as any);
  }

  private onSubmitForm = (event: React.ChangeEvent<HTMLFormElement>) => {
    event.preventDefault();
    // tslint:disable-next-line:no-console
    console.log(this.state);
    const { username, password } = this.state;
    this.props.loginUser(username, password);
  }

  private handleToggle = () => {
    this.setState({
      showPassword: !this.state.showPassword
    });
  }
}


const mapStateToProps = (state: IReduxState) => ({});

const mapDispatchToProps: IDispatchProps = {
  loginUser: userLoginRequest
};

export default connect(mapStateToProps, mapDispatchToProps)(HealthCareLogin);

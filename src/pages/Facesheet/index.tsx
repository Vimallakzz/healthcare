import React, { Component } from 'react';
import { connect } from 'react-redux';
import { push } from 'connected-react-router';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { addPatient } from '../../actions/patient';
import MenuItem from '@material-ui/core/MenuItem';
import './facesheetForm.scss';
import { IPatient } from '../../reducers/patient';
import { IReduxState } from '../../reducers';

const states = [
  {
    value: 'Florida',
    label: 'Florida'
  },
  {
    value: 'Hawaii',
    label: 'Hawaii'
  },
  {
    value: 'California',
    label: 'California'
  },
  {
    value: 'Kansas',
    label: 'Kansas'
  },
  {
    value: 'Alaska',
    label: 'Alaska'
  }
];

const cities = [
  {
    value: 'San Francisco',
    label: 'San Francisco'
  },
  {
    value: 'Sacramento',
    label: 'Sacramento'
  },
  {
    value: 'Fresno',
    label: 'Fresno'
  },
  {
    value: 'Long Beach',
    label: 'Long Beach'
  },
  {
    value: 'San jose',
    label: 'San jose'
  }
];

interface IDispatchProps {
  addPatient: (patient: IPatient) => void;
  push: any;
}

interface IFaceSheet {
    firstName: string;
    lastName: string;
    policyNumber: string;
    insurance: string;
    addressLine1: string;
    addressLine2: string;
    state: string;
    city: string;
    zipcode: string;
    redirect: boolean;
}

class FaceSheet extends Component<IDispatchProps, IFaceSheet> {
  constructor(props: IDispatchProps) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      policyNumber: '',
      insurance: '',
      addressLine1: '',
      addressLine2: '',
      state: '',
      city: '',
      zipcode: '',
      redirect: false
    };
  }

  public render() {
    const { firstName, lastName, policyNumber, insurance,
      addressLine1, addressLine2, state, city, zipcode} = this.state;
      return (
        <section>
          <div className="facesheet-form">
          <form onSubmit={this.onSubmitForm}>
              <div className="row form-head">
              {/* <span className="home">Home</span> */}
                {/* <span className="Combined-Shape">></span> */}
                <div className="done">
                <Button variant="contained" type="submit" color="primary">Done</Button>
                <Button className="cancel mr-2" type="reset">Cancel</Button>
                </div>
              </div>
              <hr className="horizontal-rule"/>
              <div className="row patientInfo-bar">
              <div className="patient-info">
                Patient Info
              </div>
              </div>
              <div className="row pt-1">
                <div className="col-xs-12 col-sm-6 col-lg-4">
                  <TextField id="outlined-basic" label=" First Name"
                    name="firstName"
                    value={firstName}
                    variant="outlined"
                    onChange={this.changeState} required={true} />
                </div>
                <div className="col-xs-12 col-sm-6 col-lg-4">
                  <TextField
                    id="outlined-basic"
                    label="Last name"
                    name="lastName" value={lastName}
                    variant="outlined"
                    onChange={this.changeState} required={true} />
                </div>
                <div className="col-xs-12 col-sm-6 col-lg-4">
                  <TextField
                    id="outlined-basic"
                    label="Policy number"
                    name="policyNumber" value={policyNumber}
                    variant="outlined"
                    onChange={this.changeState} required={true} />
                </div>
                <div className="col-xs-12 col-sm-6 col-lg-4">
                  <TextField
                    id="outlined-basic"
                    label="Insurance"
                    name="insurance" value={insurance}
                    variant="outlined"
                    onChange={this.changeState} required={true} />
                </div>
                <div className="col-xs-12 col-sm-6 col-lg-4">
                  <TextField
                    id="outlined-basic"
                    label="Address Line 1"
                    name="addressLine1" value={addressLine1}
                    variant="outlined"
                    onChange={this.changeState} required={true} />
                </div>
                <div className="col-xs-12 col-sm-6 col-lg-4">
                  <TextField
                    id="outlined-basic"
                    label="Address Line 2"
                    name="addressLine2" value={addressLine2}
                    variant="outlined"
                    onChange={this.changeState} required={true} />
                </div>
              <div className="col-xs-12 col-sm-6 col-lg-4">
                <TextField
                  id="outlined-basic"
                  select={true}
                  onChange={this.changeState}
                  label="Choose state"
                  margin="normal"
                  name="state"
                  value={state}
                  variant="outlined" required={true}
                >
                {states.map(option => (
                  <MenuItem key={option.value} value={option.value}>
                  {option.label}
                  </MenuItem>
                ))}
                </TextField>
                </div>
                <div className="col-xs-12 col-sm-6 col-lg-4 city-wrapper">
                  <TextField
                    id="outlined-basic"
                    select={true}
                    onChange={this.changeState}
                    label="Choose city"
                    margin="normal"
                    name="city"
                    value={city}
                    variant="outlined"
                  >
                  {cities.map(option => (
                    <MenuItem key={option.value} value={option.value}>
                    {option.label}
                    </MenuItem>
                  ))}
                  </TextField>
                </div>
                <div className="col-xs-12 col-sm-6 col-lg-4">
                <TextField
                  id="outlined-basic"
                  label="Zip code"
                  name="zipcode" value={zipcode}
                  variant="outlined"
                  onChange={this.changeState} required={true} />
                </div>
              </div>
            </form>
          </div>
        </section>
      );
  }

  private changeState = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    this.setState({
      [name]: value
    } as any);
  }

  private onSubmitForm = (event: React.ChangeEvent<HTMLFormElement>) => {
    event.preventDefault();
    if ((window as any).sendMessageToConversation) {
      const message = `Thank you for the discharge referral and the patient '${this.state.firstName}' has been created`;
      (window as any).sendMessageToConversation('nandhakrishnan@ideas2it.com', message);
    }
    this.props.addPatient({ ...this.state });
    this.props.push('/patients');
  }
}

const mapDispatchToProps = {
  addPatient,
  push
};

export default connect<{}, IDispatchProps, {}, IReduxState>(null, mapDispatchToProps)(FaceSheet);

import React from 'react';
import { connect } from 'react-redux';

import { userLoginRequest } from '../../actions/user';
import { IReduxState } from '../../reducers';
import styles from './login.module.scss';

interface ILoginState {
  email: string;
  password: string;
  [propName: string]: string;
}

interface IDispatchProps {
  userLogin: (email: string, password: string) => void;
}

class Login extends React.Component<IDispatchProps, ILoginState> {
  public state: ILoginState = {
    email: '',
    password: ''
  };

  public render() {
    const { email, password } = this.state;
    return (
      <section className={styles.login}>
        <header className={styles.loginHeader}>
          <h1>Login</h1>
          <small>Login to access your profile, settings & accounts.</small>
        </header>
        <form onSubmit={this.onSubmitUser} className={styles.loginForm}>
          <div className="form-group">
            <label htmlFor="email">Email</label>
            <input
              type="text"
              className="form-control"
              value={email}
              name="email"
              onChange={this.changeState}
            />
          </div>
          <div className="form-group">
            <label htmlFor="password">Password</label>
            <input
              type="password"
              className="form-control"
              value={password}
              name="password"
              onChange={this.changeState}
            />
          </div>
          <footer className={styles.loginFooter}>
            <button className="btn btn-primary" type="submit">Login</button>
          </footer>
        </form>
      </section>
    );
  }

  /**
   * Handles the user login during form submission.
   * It gets the form values from state object.
   */
  private onSubmitUser = (e: React.FormEvent) => {
    e.preventDefault();
    const { email, password } = this.state;
    this.props.userLogin(email, password);
  }

  /**
   * To update the state value for given key
   */
  private changeState = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    this.setState({
      [name]: value
    });
  }
}

const mapDispatchToProps: IDispatchProps = {
  userLogin: userLoginRequest
};

export default connect<{}, IDispatchProps, {}, IReduxState>(null, mapDispatchToProps)(Login);
